all:		geometrica.o retangulo.o triangulo.o main.o quadrado.o
		g++ -o main geometrica.o retangulo.o triangulo.o quadrado.o main.o

geometrica.o:	geometrica.cpp geometrica.hpp
		g++ -c geometrica.cpp

retangulo.o:	retangulo.cpp retangulo.hpp
		g++ -c retangulo.cpp

triangulo.o:	triangulo.cpp triangulo.hpp
		g++ -c triangulo.cpp

quadrado.o:	quadrado.cpp quadrado.hpp
		g++ -c quadrado.cpp

main.o:		main.cpp retangulo.hpp triangulo.hpp
		g++ -c main.cpp

clean:
		rm -rf *.o
		rm -rf *.cpp~
		rm -rf *.hpp~

run:
		./main
