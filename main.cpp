#include "retangulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"

#include <iostream>

using namespace std;

int main(){

	geometrica * formaGeometrica = new retangulo(20,25);
	cout << "Calculo da Area do retangulo " << formaGeometrica->getBase() << "x"<< formaGeometrica->getAltura() << " = "<< formaGeometrica->area() << endl; 

	geometrica * formaGeometrica2 = new triangulo(30,20);
	cout << "Calculo da Area do triangulo "<< formaGeometrica2->getBase() << "x"<< formaGeometrica2->getAltura() << " = "<< formaGeometrica2->area() << endl;

	geometrica * formaGeometrica3 = new quadrado(10);
	cout << "Calculo da Area do quadrado "  << formaGeometrica3->getBase() << "x"<< formaGeometrica3->getAltura() << " = "<< formaGeometrica3->area() << endl;

    return 0;
}
